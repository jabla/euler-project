def is_prime (a):
    if a > 1:
        for n in range(2,(a//2)):
            if a%n == 0:
                return False
        return True
    else: return False


n = 600851475143
dividers = []

while not(is_prime(n)):
    for i in range(3, (n//2),2):
        if is_prime(i):
            if n%i == 0:
                n = n/i
                dividers.append(i)
                print(i)

