def multiples_sum (i):
    sum = 0
    for a in range(1,i):
        if a % 3 == 0 or a % 5 == 0:
            sum += a
    return sum

print(multiples_sum(1000))
