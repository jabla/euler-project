def factorial(n):
    fact = 1
    for i in range(1,n+1):
        fact = fact*i
    return fact
def comb(n,r):
    return (factorial(n)/(factorial(r)*(factorial(n-r))))


counter = 0
for n in range(1,101):
    for r in range(1,n):
        if comb(n,r) > 1000000:
            counter += 1
print(counter)
