# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

# Find the largest palindrome made from the product of two 3-digit numbers.

def palindrome(x):
    reverse = 0
    while x != 0:
        reverse = reverse *10 + x%10
        x = x //10
    return reverse
largest_palindrome = 0
for i in range (1000,100,-1):
    for j in range (1000,100,-1):
        if i*j == palindrome (i* j):
            if largest_palindrome < i*j:
                largest_palindrome = i*j

print(largest_palindrome)
