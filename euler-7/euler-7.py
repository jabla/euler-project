def is_prime(n):
  if n > 1:
    for a in range(2,(n//2)+1):
      if n%a == 0:
        return False
    return True
  else: return False
max_it = 10001
i = 1
while max_it >0:
    i +=1
    if is_prime(i):
        max_it = max_it-1
print(i)
