def fib_even (max):
    sum = 0
    fib_0 = 1
    fib_1 = 2
    while fib_0 < max:
        tmp = fib_0
        fib_0 = fib_1
        fib_1 = fib_1 + tmp
        if fib_0 % 2 == 0:
            sum += fib_0
    return sum
print(fib_even(4000000))


# 1 2 3 5 8 13
